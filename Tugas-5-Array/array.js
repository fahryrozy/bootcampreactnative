// Code di sini
console.log('Tugas 5 – Array');
console.log('====================');

console.log('Soal No. 1 (Range)');
const range = (x, y) => {
    let arr = [];
    if(x&&y) {
        if(y>x) {
            while(x<=y) {
                arr.push(x);
                x++;
            }
        }
        else {
            while(x>=y) {
                arr.push(x);
                x--;
            }
        }
        return arr;
    }
    else {
        return -1
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 



console.log('\nSoal No. 2 (Range with Step)');
// Code di sini
const rangeWithStep = (x, y, z) => {
    let arr = [];
    if(x&&y) {
        if(y>x) {
            while(x<=y) {
                arr.push(x);
                x += z;
            }
        }
        else {
            while(x>=y) {
                arr.push(x);
                x -= z;
            }
        }
        return arr;
    }
    else {
        return -1
    }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 



console.log('\nSoal No. 3 (Sum of Range)');
// Code di sini
const sum = (x, y, z) => {
    let sum = 0;
    if(x&&y&&z) { //check if all params are exist
        if(y>x) {
            while(x<=y) {
                sum += x;
                x += z;
            }
        }
        else {
            while(x>=y) {
                sum += x
                x -= z;
            }
        }
        return sum;
    }
    else if(x&&y) { //check if firs param and second param are exist
        if(y>x) {
            while(x<=y) {
                sum += x;
                x++;
            }
        }
        else {
            while(x>=y) {
                sum += x
                x--;
            }
        }
        return sum;
    }
    else if(x) {
        return 1;
    }
    else {
        return 0;
    }
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


console.log('\nSoal No. 4 (Array Multidimensi)');
// Code di sini
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
const dataHandling = (input) => {
    let output = input.map((item) => {
        return ({ 'Nomor_ID':item[0], 'Nama_Lengkap' : item[1], 'TTL' : item[2] +' '+item[3], 'Hobby' : item[4] })
    })
    return output;
}
console.log(dataHandling(input));



console.log('\nSoal No. 5 (Balik Kata)');
// Code di sini
const balikKata = (str) => {
    let output = '';
    for(let i=str.length-1; i>=0; i--) {
        output += str[i];
    }
    return output;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 



console.log('\nSoal No. 6 (Metode Array)');
// Code di sini
let input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2 = (input) => {
    let name = input[1];
    input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input.splice(4, 1, "Pria", "SMA Internasional Metro");

    let date = input[3].split('/')
    let month;
    switch(date[1]) {
        case '01' : month='Januari'; break;
        case '02' : month='Februari'; break;
        case '03' : month='Maret'; break;
        case '04' : month='April'; break;
        case '05' : month='Mei'; break;
        case '06' : month='Juni'; break;
        case '07' : month='Juli'; break;
        case '08' : month='Agustus'; break;
        case '09' : month='September'; break;
        case '10' : month='Oktober'; break;
        case '11' : month='November'; break;
        case '12' : month='Desember'; break;
    }
    
    console.log(input);
    console.log(month);
    
    let sorted = date.sort((a,b)=>b-a);
    console.log(sorted);

    console.log(input[3].split('/').join('-'))

    return name;
} 

console.log(dataHandling2(input2));