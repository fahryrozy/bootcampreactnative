// Code di sini
console.log('Tugas 6 – Object Literal');
console.log('====================');

console.log('Soal No. 1 (Array to Object)');

function arrayToObject(arr) {
    if(arr.length>0) {
        let obj = [];
        arr.map((item, i) => {
            let now = new Date()
            let thisYear = now.getFullYear()
    
            if(item[3]) {
                item[3]<thisYear ? item[3]=thisYear-item[3] : item[3]='Invalid birth year'
            }
            else {
                item[3] = 'Invalid birth year'
            }
    
            let name = i+1 + '. ' + item[0] + ' ' + item[1];
            let detail = {'firstName': item[0], 'lastName': item[1], 'gender': item[2], 'age': item[3]};
            obj[name] = detail; 
        });
        console.log(obj);
        return obj;
    }
    else {
        let obj = " "
        console.log(obj);
        return obj;
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


console.log('\nSoal No. 2 (Shopping Time)');
let shoppingList = [
    {item: 'Sepatu Stacattu', price: 1500000},
    {item: 'Baju Zoro', price:500000},
    {item: 'Baju H&N', price: 250000},
    {item: 'Sweater Uniklooh', price:175000},
    {item: 'Casing Handphone', price: 50000}
]
function shoppingTime(memberId, money) {
    const lowestPrice = Math.min(...shoppingList.map(item => parseInt(item.price)));

    if(!memberId) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    }
    else {
        if(money>lowestPrice) {
            let obj = {}
            let buy = [];
            obj['memberId'] = memberId;
            obj['money'] = money;
            shoppingList.forEach(item => {
                if(money>=item.price) {
                    buy.push(item.item)
                    money-=item.price;
                }
                else {
                    return "Mohon maaf, uang tidak cukup";
                }
            })
            obj['listPurchased']=buy;
            obj['changeMoney']=money;
            return obj;
        }
        else {
            return "Mohon maaf, uang tidak cukup";
        }
    }
  }
   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


console.log('\nSoal No. 3 (Naik Angkot)');
function naikAngkot(arrPenumpang) {
rute = ['A', 'B', 'C', 'D', 'E', 'F'];
let output = [];
arrPenumpang.forEach(item => {
    let obj = {};
    let from = rute.indexOf(item[1])+1
    let to = rute.indexOf(item[2])+1
    let tarif = (to-from) * 2000;
    obj['penumpang']=item[0];
    obj['naikDari']=item[1];
    obj['tujuan']=item[2];
    obj['bayar']=tarif;
    output.push(obj);
})
return output;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]