var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]


let i = 0;
function checkSisaWaktu(time){
    if(i < books.length) {
        i++;
        readBooksPromise(time, (i!=books.length)?books[i]:books[0]).then(function (fulfilled) {
            checkSisaWaktu(fulfilled);
        })
        .catch(function (error) {
            // console.log(error);
        });
    }
}

readBooksPromise(10000, books[i]).then(function (fulfilled) {
    checkSisaWaktu(fulfilled);
})
.catch(function (error) {
    // console.log(error);
});
