import React, {useState, useMemo, useEffect, useReducer} from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import * as URL from './../services/url';
import { useSelector } from 'react-redux';
// import axios from './../services/api';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/dist/FontAwesome5';

import Welcome from '../screens/Welcome';
import Home from '../screens/Home';
import Login from '../screens/Login';
import Register from '../screens/Register';
import Account from '../screens/Account';
import Course from '../screens/Course';
import Scoreboard from '../screens/Scoreboard';
import Detail from '../screens/Detail';
import Splash from '../screens/Splash';
import Notification from './../screens/Notification';
import Cart from './../screens/Cart';
import Unauthorized from './../screens/Unauthorized'
import AuthContext from './../services/context';
import { useDispatch } from 'react-redux';
import { login, logout } from '../services/redux/action/authentication';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const HomeStackScreen = () => (
    <Stack.Navigator headerMode="none">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Notification" component={Notification} />
        <Stack.Screen name="Cart" component={Cart} />
        <Stack.Screen name="Detail" component={Detail} />
    </Stack.Navigator>
)
const UnauthorizedStackScreen = () => (
    <Stack.Navigator headerMode="none">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Notification" component={Unauthorized} />
        <Stack.Screen name="Cart" component={Unauthorized} />
        <Stack.Screen name="Detail" component={Detail} />
    </Stack.Navigator>
)

const MainTabScreen = () => (
        <Tab.Navigator tabBarOptions={{
            activeTintColor: '#003366',
          }}>
            <Tab.Screen name="Home" component={HomeStackScreen} options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color, size }) => (
                    <Ionicon name="home" color={color} size={size} />
                ),
                }} />
            <Tab.Screen name="My Course" component={Course}  options={{
                tabBarLabel: 'My Course',
                tabBarIcon: ({ color, size }) => (
                    <Ionicon name="school" color={color} size={size} />
                ),
                }} />
            <Tab.Screen name="Scoreboard" component={Scoreboard}  options={{
                tabBarLabel: 'Scoreboard',
                tabBarIcon: ({ color, size }) => (
                    <FontAwesome5 name="laptop-code" color={color} size={size} />
                ),
                }} />
            <Tab.Screen name="Account" component={Account}  options={{
                tabBarLabel: 'Account',
                tabBarIcon: ({ color, size }) => (
                    <Ionicon name="person" color={color} size={size} />
                ),
                }} />
        </Tab.Navigator>
)


const UnauthorizedTabScreen = () => (
    <Tab.Navigator tabBarOptions={{
        activeTintColor: '#003366',
      }}>
        <Tab.Screen name="Home" component={UnauthorizedStackScreen} options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ color, size }) => (
                <Ionicon name="home" color={color} size={size} />
            ),
            }} />
        <Tab.Screen name="My Course" component={Unauthorized}  options={{
            tabBarLabel: 'My Course',
            tabBarIcon: ({ color, size }) => (
                <Ionicon name="school" color={color} size={size} />
            ),
            }} />
        <Tab.Screen name="Scoreboard" component={Unauthorized}  options={{
            tabBarLabel: 'Scoreboard',
            tabBarIcon: ({ color, size }) => (
                <FontAwesome5 name="laptop-code" color={color} size={size} />
            ),
            }} />
        <Tab.Screen name="Account" component={Unauthorized}  options={{
            tabBarLabel: 'Account',
            tabBarIcon: ({ color, size }) => (
                <Ionicon name="person" color={color} size={size} />
            ),
            }} />
    </Tab.Navigator>
)

const Navigator = () => {
    const [isLoading, setisLoading] = useState(true);
    const token = useSelector((state) => state.token);
    const dispatch = useDispatch();
    const loginUser = (data, token) => dispatch(login(data, token));
    const logoutUser = () => dispatch(logout());
    const authContext = useMemo(() => {
        return {
          signIn: async (data, storageKey, nav) => {
            try {
                const getAuth = await fetch(URL._LOGIN_, {
                  method: 'POST', // *GET, POST, PUT, DELETE, etc.
                  headers: {
                    'Content-Type': 'application/json'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                  },
                  body: JSON.stringify(data), // body data type must match "Content-Type" header
                });
                const tokenAuth = await getAuth.json();
                console.log(JSON.stringify(tokenAuth.token));
                if(getAuth.status === 200) {
                    alert('Login success');
                    loginUser(data, tokenAuth.token);
                    AsyncStorage.setItem(storageKey, tokenAuth.token);
                    nav.reset({
                      index: 0,
                      routes: [{ name: 'Main', Screen: 'Home' }],
                    });

                }
                else {
                    alert('Login failed');
                }
              } catch (error) {
                console.log(error)
              }
          },
          signUp: async (data, storageKey, nav) => {
            const getReg = await fetch(URL._REGISTER_, {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                headers: {
                  'Content-Type': 'application/json'
                  // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: JSON.stringify(data), // body data type must match "Content-Type" header
              });
              console.log(getReg)
              const tokenReg = await getReg.json();
              const credUser = JSON.stringify(tokenReg);
              console.log(credUser)
              if(getReg.status === 201) {
                AsyncStorage.setItem(storageKey, credUser);
                alert('Register Succes');
                nav.reset({
                    index: 0,
                    routes: [{ name: 'Login', Screen: 'Login' }],
                });
              }
              else {
                  alert('Register failed')
              }
          },
          signOut: () => {
            logoutUser();
          }
        };
      }, []);

    useEffect(()=>{
        setTimeout(()=>{
            console.log(token)
            setisLoading(false)
        }, 2000)
    }, [])

    if(isLoading) {
        return <Splash />
    }


    return (
        <AuthContext.Provider value={authContext}>
        <NavigationContainer>
            {token === null ? (
                    <Stack.Navigator headerMode="none" initialRouteName="Welcome">
                        <Stack.Screen name="Welcome" component={Welcome} />
                        <Stack.Screen name="Main" component={UnauthorizedTabScreen} />
                        <Stack.Screen name="Login" component={Login} />
                        <Stack.Screen name="Register" component={Register} />
                    </Stack.Navigator>
                ): (
                    
                    <Stack.Navigator headerMode="none" initialRouteName="Main">
                        <Stack.Screen name="Main" component={MainTabScreen} />
                        <Stack.Screen name="Login" component={Login} />
                        <Stack.Screen name="Register" component={Register} />
                    </Stack.Navigator>
                )
            }

        </NavigationContainer>
        </AuthContext.Provider>
    )
}

export default Navigator;
