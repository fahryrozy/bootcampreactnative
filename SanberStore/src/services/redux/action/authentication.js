export const login = (auth, token) => ({
    type: 'LOGIN',
    payload: auth,
    token: token,
});

export const logout = () => ({
    type: 'LOGOUT',
});

export const register = (auth) => ({
    type: 'REGISTER',
    payload: auth,
});