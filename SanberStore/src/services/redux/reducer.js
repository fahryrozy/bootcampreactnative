const initialState = {
    auth: {},
    token: null,
    numOfItem: 0,
    items: [],
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TO_CART':
            console.log('Add')
            console.log(state.items)
            return {
                ...state,
                numOfItem: state.numOfItem+1,
                items: [...state.items, action.payload],
            }
        
        case 'REMOVE_FROM_CART':
            return {
                ...state,
                numOfItem: state.numOfItem-1,
                items: state.items.filter(item => item.id !== action.payload)
            }
        case 'LOGIN':
            console.log(action.token);
            return {
                ...state,
                auth: action.auth,
                token: action.token,
                shop: {
                    numOfItem: 0,
                    items: [],
                    total: 0,
                }
            }
        case 'LOGOUT':
            console.log('LOGOUT')
            return {
                ...state,
                auth: {},
                token: null,
                shop: {
                    numOfItem: 0,
                    items: [],
                    total: 0,
                }
            }
        default:
            return state
    }
}