import React, {useState} from 'react'
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { AddButton, RemoveButton } from './Button';
import { addToCart, removeFromCart } from '../services/redux/action/item';
import { useDispatch } from 'react-redux';

const DEVICE = Dimensions.get('window');


const ListProducts = ({data, nav}) => {
    const [isItemAdded, setisItemAdded] = useState(false);

    const dispatch = useDispatch();

    const addItem = (item) => dispatch(addToCart(item));
    const removeItem = (itemId) => dispatch(removeFromCart(itemId))
    // console.log(itemAdded);
    return (
      <View style={styles.container}>
      <TouchableOpacity onPress={()=>nav.navigate('Detail')}>
        <View style={styles.itemContainer}>
                    <Image
                    source={{ uri: data.item.gambaruri }}
                    style={styles.itemImage}
                    resizeMode="contain"
                    />
                    <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemName}>
                    {data.item.nama}
                    </Text>
          </View>
      </TouchableOpacity>
      <View style={{flexDirection: 'row', width: DEVICE.width*0.45, justifyContent: 'center', padding: 10, }}>
        {!isItemAdded && 
          <AddButton 
          onPress={()=> {
              setisItemAdded(true)
              addItem(data.item)
              // this.props.add(data)
            }
          } 
          />
        }
        {isItemAdded && 
          <RemoveButton 
          onPress={()=> {
            setisItemAdded(false)
            removeItem(data.item.id)
            } 
          }
          />
        }
        {/* <ShopButton onPress={isItemAdded?setisItemAdded(false):setisItemAdded(false)} title={isItemAdded?'Remove':'Add'} /> */}
      </View>
    </View>
       
    )
}

const Product = ({data, navigation}) => {
    return (
        <FlatList data={data.produk}
         renderItem={item => <ListProducts data={item} nav={navigation}/>}
         numColumns='2'
         keyExtractor={item => item.id} 
         showsVerticalScrollIndicator ={false}
         showsHorizontalScrollIndicator={false}
        />
    )
}

export default Product;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    headerText: {
      fontSize: 18,
      fontWeight: 'bold',
    },
    container: {
      width: DEVICE.width * 0.45,
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: '#ffffff',
      elevation: 1,
      marginHorizontal: 5,
      marginVertical: 5,
      paddingHorizontal: 5,
      paddingVertical: 20,
    },
    itemContainer: {
      width: DEVICE.width * 0.45,
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: '#ffffff',
      marginHorizontal: 5,
      marginVertical: 5,
    },
    itemImage: {
      width: '90%',
      height: 100,
    },
    itemName: {
      paddingVertical: 10,
      paddingHorizontal: 5,
      fontSize: 12,
      textAlign: 'center',
      fontWeight: 'bold',
    },
    itemPrice: {
      color: 'blue',
      padding: 10,
    },
    itemStock: {
      fontWeight: 'bold',
    },
    itemButton: {
      padding: 5,
    },
    buttonText: {
      padding: 5,
    },
  });