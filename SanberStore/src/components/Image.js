import React from 'react';
import {
  StyleSheet,
  Image,
  Dimensions,
} from 'react-native';

const { width, height } = Dimensions.get("window");

export const WorkSpace = () => {
    const logo = require('./../assets/image/workspace.png')
    return (
        <Image style={styles.logo} source={logo} />
    )
}

export const SanberLogo1 = () => {
  const logo = require('./../assets/image/sanbercodelogo1.jpg')
  return (
      <Image style={styles.welcomelogo} source={logo} />
  )
}

export const SanberLogo2 = () => {
  const logo = require('./../assets/image/sanbercodelogo2.png')
  return (
      <Image style={styles.logo} source={logo} />
  )
}

export const UnderDevelopment = () => {
  const logo = require('./../assets/image/underdevelopment.jpg')
  return (
      <Image style={styles.logo} source={logo} />
  )
}
  
export const Unauthorized = () => {
  const logo = require('./../assets/image/unauthorized.png')
  return (
      <Image style={styles.logo} source={logo} />
  )
}
const styles = StyleSheet.create({
    logo: {
        marginTop: 25,
        padding: 10,
        justifyContent: 'center',
        alignContent: 'center',
    },
    welcomelogo: {
      marginTop: 25,
      padding: 10,
      justifyContent: 'center',
      alignContent: 'center',
      width: width*0.6,
      height: height*0.15,
  },
    imageBackground: {
      flex: 1,
      resizeMode: 'cover',
      justifyContent: 'center',
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },
    unauthorized: {
      resizeMode: 'contain',
      justifyContent: 'center',
      position: 'absolute',
    }
});
  