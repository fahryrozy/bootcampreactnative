import React from 'react'
import { View, Text, Image, Dimensions, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native'
import { WorkSpace, SanberLogo2, SanberLogo1 } from './../components/Image';
import { LoginButton, LinkButton } from './../components/Button';

const { width, height } = Dimensions.get("window");
// const image = require('../assets/background/background.PNG');

const Welcome = ({navigation}) => {
    return (
        <View style={styles.container}>
        <ImageBackground source={require('../assets/background/background.png')} style={styles.imageBackground}>
        <View style={styles.topContainer}>
          <SanberLogo1 />
          <Text style={styles.label}>Ngoding Santai  Berkualitas</Text>
          <Text style={styles.p}>
          Kami membantu Anda belajar intensif membuat website, mobile apps, data science, desain grafis, dan lainnya.
          </Text>
          <Text style={styles.p}>Kami bantu Anda mendapatkan kerja / proyek.</Text>
          <WorkSpace />
        </View>
        <View style={styles.bottomContainer}>
          <LoginButton title="Masuk" onPress={()=>navigation.navigate('Login')} />
          <LinkButton title="Lanjutkan tanpa tercatat" style={{marginVertical: 10}} onPress={()=>navigation.replace('Main')} />
        </View>
      </ImageBackground>
        </View>
      )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },  
  
  topContainer: {
    flex: 4,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },    
  bottomContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: height/10,
  }, 
  imageBackground: {
    flex: 1,
    resizeMode: "cover",
    alignItems: 'center'
  },
  logo: {
    width: width/2,
    height: height/10,
  },
  label: {
    padding: 10,
    fontWeight: 'bold',
    fontSize: 23,
    color: '#3EC6FF',
    marginVertical: 10,
  },
  p: {
    fontSize: 14,
    color: '#003366',
    paddingHorizontal: 20,
    textAlign: 'center',
  }
})


export default Welcome;
