import React from 'react'
import { View, Text } from 'react-native'
import { GoBack } from '../components/Button'
import { Unauthorized } from './../components/Image';

const Scoreboard = ({navigation}) => {
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems:'center', backgroundColor: '#FFF'}}>
            <Unauthorized />
            <GoBack onPress={()=>navigation.goBack()} />
        </View>
    )
}

export default Scoreboard
