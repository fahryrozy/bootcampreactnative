import React, {useContext} from 'react'
import { View, Text, StyleSheet, SafeAreaView } from 'react-native'
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome5';
import { Logout } from '../components/Button';
import AuthContext from './../services/context';

const Account = ({navigation}) => {
    const { signOut } = useContext(AuthContext);
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.topContainer}>
                <Text style={styles.label}>Tentang Saya</Text>
                <Ionicons name="person-circle" size={150} color="#EFEFEF" />
                <Text style={styles.title}>Fahry Rozy Siregar</Text>
                <Text style={styles.text}>React Native Developer</Text>
                <Logout onPress={()=>{
                    signOut();
                    navigation.reset({
                      index: 0,
                      routes: [{ name: 'Login', Screen: 'Login' }],
                    });

                  } 
                }/>
            </View>
            <View style={styles.socialContainer}>
                <View style={styles.sectionTitle}>
                    <Text style={styles.darkBlue}>Hubungi saya</Text>
                </View>
                <View style={styles.socialIcon}>
                    <View style={styles.socialInfo}>
                        <FontAwesome name="facebook-square" size={24} color="#3EC6FF" />
                        <Text style={styles.darkBlue}>Fahry Rozy Siregar</Text>
                    </View>
                    <View style={styles.socialInfo}>
                        <FontAwesome name="instagram" size={24} color="#3EC6FF" />
                        <Text style={styles.darkBlue}>@fahryrozy</Text>
                    </View>
                    <View style={styles.socialInfo}>
                        <FontAwesome name="linkedin" size={24} color="#3EC6FF" />
                        <Text style={styles.darkBlue}>Fahry Rozy Siregar</Text>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default Account;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    topContainer: {
      alignItems: 'center',
      flex: 3,
      width: '100%',
      marginVertical: '20%',
    },    
    portFolioContainer: {
      alignItems: 'center',
      justifyContent: 'space-evenly',
      backgroundColor: '#EFEFEF',
      flex: 1,
      width: '90%',
      borderRadius: 10,
      paddingHorizontal: 20,
      paddingVertical: 10,
      marginVertical: 10,
    },   
    socialContainer: {
      alignItems: 'center',
      backgroundColor: '#EFEFEF',
      flex: 2,
      width: '90%',
      borderRadius: 10,
      padding: 20,
      marginVertical: 10,
    },     
    textInput: {
        borderStyle: 'solid',
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderWidth: 1,
        width: 250,
        marginHorizontal: 10,
        marginVertical: 10,
        color: 'black',
    },
    label: {
      color: '#003366',
      fontSize: 36,
      fontWeight: 'bold',
      textAlign: 'center',
      fontFamily: 'Roboto',
    },
    title: {
      color: '#003366',
      fontSize: 22,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    text: {
      color: '#3EC6FF',
      fontSize: 14,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    sectionTitle: { 
        padding: 10,
        marginBottom: 20,
        borderBottomWidth: 2,
        borderColor: '#003366',
        width: '100%',
        flex: 1,
    },
    portoFolioIcon: {
        flexDirection: 'row',
        flex: 3,
        justifyContent: 'space-around',
        width: '100%',
        alignItems: 'center',
        marginTop: 5,
    },
    portoFolioInfo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    socialIcon: {
        width: '60%',
        flex: 6,
        justifyContent: 'space-evenly',
        marginTop: 25,
    },
    socialInfo: {
        flex: 1,
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
    darkBlue: {
        color: '#003366',
        fontWeight: 'bold',
    }
  });
  