import React, {useState, useContext} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ImageBackground,
} from 'react-native';
import { LoginButton, LinkButton } from './../components/Button';
import { SanberLogo2 } from './../components/Image';
import AuthContext from './../services/context';

// const loginHandler = (navigation) => {
//   navigation.reset({
//     index: 0,
//     routes: [{ name: 'Main', Screen: 'Home' }],
//   });
// }

const Login = ({navigation}) => {
  const [auth, setauth] = useState();
  const { signIn } = useContext(AuthContext);
    return (
        <ImageBackground source={require('../assets/background/bg-auth.jpg')} style={styles.imageBackground}>
            <View style={styles.topContainer}>
              <SanberLogo2 />
              <View>
                  <Text style={styles.title}>Login</Text>
                  <TextInput 
                    style={styles.textInput}
                    name="email"
                    // value={auth.email}
                    onChangeText={(val)=>setauth({...auth, email: val})}
                    placeholder="Email Address" 
                    placeholderTextColor="black" />

                  <TextInput 
                    style={styles.textInput} 
                    name="password"
                    // value={auth.password}
                    onChangeText={(val)=>setauth({...auth, password: val})}
                    placeholder="Password" 
                    secureTextEntry={true}  
                    placeholderTextColor="black" />

                  <LinkButton style={{textAlign: 'right', marginVertical: 20, paddingHorizontal: 20,}} title="Lupa kata sandi?" />
                  <LoginButton title="Login" style={{width: 300}} 
                    onPress={() => {
                      signIn(auth, '@authUser', navigation)
                      // setAuth(navigation, auth)
                     } 
                    }
                  />
              </View>
            </View>
            <View style={styles.bottomContainer}>
                <LinkButton style={{color: '#003366' }} title="Belum memiliki akun? Daftar Sekarang" 
                  onPress={() => navigation.navigate('Register')} />
            </View>
        </ImageBackground>
    )
}

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  topContainer: {
    flex: 11,
    alignItems: 'center',
    justifyContent: 'center',
  },    
  bottomContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 50,
  },   
  imageBackground: {
    flex: 1,
    resizeMode: "cover",
    alignItems: 'center'
  },
  textInput: {
    borderRadius: 10,
      backgroundColor: '#eee',
      borderStyle: 'solid',
      paddingVertical: 5,
      borderWidth: 1,
      width: 300,
      marginHorizontal: 10,
      marginVertical: 10,
      color: 'black',
      textAlign: 'center',
  },
  label: {
    color: '#3EC6FF',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'right',
    alignSelf: 'flex-end',
  },
  title: {
    color: '#003366',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 30,
  },
  text: {
    color: '#3EC6FF',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
