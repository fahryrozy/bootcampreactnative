import React from 'react'
import { View, Text, TextInput, StyleSheet, Dimensions, Image, Button } from 'react-native'
import Product from "./../components/Product";
import { useSelector } from 'react-redux';
import IconBadge from 'react-native-icon-badge';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';

import data from '../services/data.json';
import { TouchableOpacity } from 'react-native-gesture-handler';

const DEVICE = Dimensions.get('window');
const Home = ({navigation}) => {
  const item = useSelector((state) => state.numOfItem.toString());
  console.log(item)
    return (
        <View style={styles.container}>
            <View style={styles.carouselContainer}>
                <View style={styles.panelHeader}>
                    <View style={styles.searchBar}>
                        <Ionicon name="search" size={23} color="black" style={{padding: 3}} />
                        <TextInput placeholder="Search" />
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent:'flex-end', marginLeft: 10,}}>
                      <TouchableOpacity onPress={()=>navigation.navigate('Notification')} >
                        <Ionicon name="notifications" size={20} color="white" style={{paddingHorizontal: 4}}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>navigation.navigate('Cart')} >
                        <IconBadge 
                          MainElement={<Ionicon name="cart" size={23} color="white" style={{paddingHorizontal: 4}}/>}
                          BadgeElement={<Text style={{color:'#0E0000', fontSize: 10}}>{item}</Text>}
                          Hidden={item==0}
                          IconBadgeStyle={{width: 10, height: 15}}
                        />
                      </TouchableOpacity>
                    </View>
                </View>
            </View>
            <View style={styles.contentContainer}>
                <Product data={data} navigation={navigation}/>
            </View>
        </View>
    )
}

export default Home;


const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'space-evenly',
      alignItems: 'center',
    },
    carouselContainer: {
        flex: 1,
        width: DEVICE.width,
        backgroundColor: '#003366',
        paddingVertical: 15,
        paddingHorizontal: 20,
    },
    panelHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
        alignItems: 'center',
    },
    contentContainer: {
        flex: 5,
        paddingTop: 30,
        paddingBottom: 5,
        paddingHorizontal: 10,
        width: DEVICE.width,
        backgroundColor: '#FFFFFF',
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        marginTop: -30,
        elevation: 1,
    },
    searchBar: {
        flex: 3,
        flexDirection: 'row',
        paddingHorizontal: 20,
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderRadius: 10,
        width: DEVICE.width*0.65,
        height: DEVICE.height*0.05,
    },
    topContainer: {
      flex: 11,
      alignItems: 'center',
      justifyContent: 'center',
    },    
    bottomContainer: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
      marginBottom: 50,
    },   
    imageBackground: {
      flex: 1,
      resizeMode: "cover",
      alignItems: 'center'
    },
    textInput: {
        borderRadius: 10,
        backgroundColor: '#eee',
        borderStyle: 'solid',
        borderWidth: 1,
        width: 300,
        marginHorizontal: 10,
        color: 'black',
        textAlign: 'center',
    },
    label: {
      color: '#3EC6FF',
      fontSize: 16,
      fontWeight: 'bold',
      textAlign: 'right',
      alignSelf: 'flex-end',
    },
    title: {
      color: '#003366',
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center',
      marginBottom: 30,
    },
    text: {
      color: '#3EC6FF',
      fontSize: 16,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    marginHorizontal: 5,
    marginVertical: 5,
    padding: 5,
  },
  itemImage: {
    width: '90%',
    height: 100,
  },
  itemName: {
    textAlign: 'left',
    fontWeight: 'bold',
  },
  itemPrice: {
    color: 'blue',
    padding: 10,
  },
  itemStock: {
    fontWeight: 'bold',
  },
  itemButton: {
    padding: 5,
  },
  buttonText: {
    padding: 5,
  },
  });
  