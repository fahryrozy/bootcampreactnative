import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
const Splash = ({navigation}) => {
    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1, backgroundColor: '#003366'}}>
           <Image
        style={styles.tinyLogo}
        source={require('./../assets/image/logo-white.png')}
      />
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    tinyLogo: {
        flex: 1,
        resizeMode: "contain",
        alignItems: 'center'
      },
})
