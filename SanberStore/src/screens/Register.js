import React, {useState, useContext} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    ImageBackground,
} from 'react-native';
import { RegisterButton, LoginButton, LinkButton } from './../components/Button';
import { SanberLogo2 } from './../components/Image';
import AuthContext from './../services/context';


const registerHandler = ({navigation, route}) => {
  navigation.replace('Main',{Screen: 'Home'});
}

const Register = ({navigation}) => {
  const [reg, setReg] = useState({age: 30});
  const { signUp } = useContext(AuthContext);
    return (
        <ImageBackground source={require('../assets/background/bg-auth.jpg')} style={styles.imageBackground}>
            <View style={styles.topContainer}>
              <SanberLogo2 />
              <View>
                <Text style={styles.title}>Register</Text>
                <TextInput style={styles.textInput} 
                  placeholder="Nama Lengkap" 
                  placeholderTextColor="black" 
                  value={reg.name}
                  onChangeText={(val)=>setReg({...reg, name: val})} />
              
                <TextInput style={styles.textInput} 
                  placeholder="Email Address" 
                  placeholderTextColor="black" 
                  value={reg.email}
                  onChangeText={(val)=>setReg({...reg, email: val})}/>

                <TextInput style={styles.textInput} 
                  placeholder="Password" 
                  placeholderTextColor="black" 
                  value={reg.password}
                  onChangeText={(val)=>setReg({...reg, password: val})} />
                <TextInput style={styles.textInput} placeholder="Ulangi Password" placeholderTextColor="black" />
                <RegisterButton title="Register" style={{width: 300, marginVertical: 50}} 
                   onPress={()=>signUp(reg, '@regUser', navigation)} 
                />
              </View>
            </View>
            <View style={styles.bottomContainer}>
                <LinkButton style={{color: '#003366' }} title="Sudah memiliki akun? Login disini" 
                  onPress={() => navigation.goBack()} />
            </View>
        </ImageBackground>
    )
}

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  topContainer: {
    flex: 11,
    alignItems: 'center',
    justifyContent: 'center',
  },    
  bottomContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 50,
  },   
  imageBackground: {
    flex: 1,
    resizeMode: "cover",
    alignItems: 'center'
  },
  textInput: {
    borderRadius: 10,
      backgroundColor: '#eee',
      borderStyle: 'solid',
      paddingVertical: 5,
      borderWidth: 1,
      width: 300,
      marginHorizontal: 10,
      marginVertical: 10,
      color: 'black',
      textAlign: 'center',
  },
  label: {
    color: '#3EC6FF',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'right',
    alignSelf: 'flex-end',
  },
  title: {
    color: '#003366',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 30,
  },
  text: {
    color: '#3EC6FF',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
