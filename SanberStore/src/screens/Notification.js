import React from 'react'
import { View } from 'react-native'
import { GoBack } from '../components/Button'
import { UnderDevelopment } from '../components/Image'

const Notification = ({navigation}) => {
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems:'center', backgroundColor: '#FFF'}}>
            <UnderDevelopment />
            <GoBack onPress={()=>navigation.goBack()} />
        </View>
    )
}

export default Notification
