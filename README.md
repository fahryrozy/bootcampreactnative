# BootcampReactNative

Repository ini digunakan sebagai wadah untuk pengumpulan tugas selama menjalani kegiatan Bootcamp React Native.

## Daftar Isi
- [Tugas 1](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/Tugas-1-Git) 
- [Tugas 2](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/Tugas-2-String%20and%20Conditional)
- [Tugas 3](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/Tugas-3-Looping)
- [Tugas 4](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/Tugas-4-Function)
- [Tugas 5](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/Tugas-5-Array)
- [Tugas 6](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/Tugas-6-Object%20Literal)
- [Tugas 7](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/Tugas-7-ES6)
- [Tugas 8](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/Tugas-8-Asynchronous)
- [Tugas 9](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/Tugas-9-Class)
- [Tugas 10](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/SanberApp)
