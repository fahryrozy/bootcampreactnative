import React from 'react';
import YoutubeUI from "./Tugas/Tugas12/Apps";
import { Register } from "./Tugas/Tugas13/Register";
import { Login } from "./Tugas/Tugas13/Login";
import { About } from './Tugas/Tugas13/About';
import Tugas14 from './Tugas/Tugas14/App'
import { Tugas15 } from './Tugas/Tugas15/index'
import Quiz3 from './Tugas/Quiz3/index';
import REST_API from './Tugas/REST_API/index';
import StateManagement from './Tugas/State_Management/App';
import {Provider} from 'react-redux'
import { createStore } from "react-redux";
import { AppRegistry } from 'react-native';

const store = createStore();

export default function App() {
  return (
    <Provider>      
      <StateManagement store={store} />
    </Provider>
    // <YoutubeUI />
    // <Register />
    // <Login />
    // <About />
    // <Tugas14 />
  );
}

AppRegistry.registerComponent("SanberApp", () => App)
