export default (state = 0, action) => {
    switch(action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        case CLEAR_COUNTER:
            return 0;
        case SET_COUNTER:
            return action.payload;
        default:
            return state;
    }
}