import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { SignIn,
         CreateAccount, 
         Profile, 
         Home,
         Details,
         Search2,
         Search,
        } from './Screen';
import DrawerLayout from "react-native-gesture-handler/DrawerLayout";

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name='Home' component={Home} />
      <HomeStack.Screen name='Details' component={Details} options={({ route }) => {
        return ({title: route.params.name})
      }} />
    </HomeStack.Navigator>
  )
}

const SearchStackScreen = () => {
  return (
    <SearchStack.Navigator>
      <SearchStack.Screen name='Search' component={Search} />
      <SearchStack.Screen name='Search2' component={Search2} />
    </SearchStack.Navigator>
  )
}

const ProfileStackScreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen name='Profile' component={Profile} />
  </ProfileStack.Navigator> 
)

const TabScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name='Home' component={HomeStackScreen} />
    <Tabs.Screen name='Search' component={SearchStackScreen} /> 
  </Tabs.Navigator>
)

const DrawerStack = createDrawerNavigator();

export function Tugas15() {
  return (
    <NavigationContainer>
      <DrawerStack.Navigator>  
        <DrawerStack.Screen name='Home' component={TabScreen} />
        <DrawerStack.Screen name='Profile' component={ProfileStackScreen} />
      </DrawerStack.Navigator>
      {/* <AuthStack.Navigator>
        <AuthStack.Screen name='SignIn' component={SignIn} options={{title: 'Sign In'}} />
        <AuthStack.Screen name='CreateAccount' component={CreateAccount} options={{title: 'Create Account'}} />
      </AuthStack.Navigator> */}
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});