import React from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
} from 'react-native';
import { RegisterButton, LoginButton } from './components/Button';
import { AppLogo } from './components/Image';

export const Login = () => {
    return (
        <View style={styles.container}>
            <View style={styles.topContainer}>
                <AppLogo />
                <Text style={styles.label}>PORTOFOLIO</Text>
            </View>
            <View>
                <Text style={styles.title}>Login</Text>
                <TextInput style={styles.textInput} placeholder="Username" placeholderTextColor="black" />
                <TextInput style={styles.textInput} placeholder="Email" placeholderTextColor="black" />
            </View>
            <View style={styles.bottomContainer}>
                <RegisterButton title="Daftar" />
                <Text style={styles.text}>atau</Text>
                <LoginButton title="Masuk" />
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  topContainer: {
    alignItems: 'center',
  },    
  bottomContainer: {
    alignItems: 'center',
  },    
  textInput: {
      borderStyle: 'solid',
      paddingHorizontal: 15,
      paddingVertical: 5,
      borderWidth: 1,
      width: 250,
      marginHorizontal: 10,
      marginVertical: 10,
      color: 'black',
  },
  label: {
    color: '#3EC6FF',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'right',
    alignSelf: 'flex-end',
  },
  title: {
    color: '#003366',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 30,
  },
  text: {
    color: '#3EC6FF',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
