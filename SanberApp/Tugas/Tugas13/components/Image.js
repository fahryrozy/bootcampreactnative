import React from 'react';
import {
  StyleSheet,
  Image
} from 'react-native';

export const AppLogo = ({title}) => {
    const logo = require('./../assets/logos/logo_putih1.png')
    return (
        <Image style={styles.logo} source={logo} />
    )
}

export const AppBackground = ({title}) => {
    const background = {uri: 'https://images.pexels.com/photos/1591447/pexels-photo-1591447.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',};
    return (
        <Image style={styles.imageBackground} source={background} />
    )
}

export const LoginBackground = ({title}) => {
    const background = {uri: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEg8PEhIQEhUPEA8PEBUPDw8PDxAPFRUWFhUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGBAQGi0dHR8tLS0tLS0rLS0tKy0tLS0tLS0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIASsAqAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAABAgADBAUGB//EAC8QAAICAQMDAwIFBAMAAAAAAAABAhEDBCExEkFRBWFxE4EUIpGhsQYywdFSguH/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQMCBAX/xAAgEQEBAAIDAQEAAwEAAAAAAAAAAQIRAxIhMUEEE2FR/9oADAMBAAIRAxEAPwDjMAzQD0HiIggoIBADAABQGggaAEaFodgoASgwhbosih8F9SS5boAGDTtve6Rtx6dWq+50tPonGm+63+TTqdKmk6/TwYuS+PFdMmk0PXTvZc+fg7ODSKKpIOgSrijo44EssnTx8cjAsG5uUF00F4xZGLdqyaUZHXAcUyjUDYR68Z361qRBLIJrb5+0Kx2gHW8opEg0GgAECSgAAYwKAEaAO0CgA44N8HV9G0EnPqapVfyc7SrdVXPdnp9Dqkvyur9uDGVuleLGW+tskkgwSe5l9Q08pJOLryjPDUvHJKWydfqS1uOq5avruY8ZoijPp8qa2NUWTq8SRTMukUSA6x5hYzSNOXFZR+E3s1tO7WQdkGwxrYgjkeEaFosoVo63mFIGg0BAiUEgGWgDgaAiijAAAjVin1Pnppbb8tGYlAJXpdJ6jGnFvdbbsbVY3JJtbXaZ5vE9+aOth9Qn0142+Sdx18dGPLuarbhzdG6b7HV0uui9rPP5NR1R32J6fk/NvujNx23jyaunq1ksjkYfrbKhPxLS8k+ro7t6HUTDptX1Oqo29YrGpZVsYkK1mILTW48BQKCgnY8knSSi7FjcmopW34Osv6fyNRaat83wvgVyk+tY4ZZfI4dAo6mo9Eywi5NJpK3T3r4Oa0Esvwssbj9IBjNCjZAgSJAC0Sh6JQAtF+DNSp+f2KSAJ47Us+OkkueaK1krqr7PsmctSZYsr88meqnd2cHqKaSl28FsdRHlb/6OFL5Jim+RdY1OWu7HWJN7mzBro+fbfuec6tlzvd15sEZ77iuEanNY9LLM29t/ghy8OtS7EM9VP7J/1x0a/T9G8k1FcXu+1Fno+mU8lNWj22l0sYpUl+iNZ59fE+Hh7+1VofTIQ/tSXmu50OhIdIqyM5t7ejJJPGHXydSr/i/hnhc+GS3lFx6m6tV8nvs7SPI+vuTyK+KqNcItxX8cn8nHzbkMUsaEou4SkGoFAERKIEAUgWACQJCADRZZLIt2tvb+SkgHtpxzdN/YqchUwANtKnaCZ7ILR7dPQ5nhn1qnFvpbPY6LUqaTXc8FDI+nppb+256z0aHTBX7UyXLP11/x878/Hec0Z8mQV2ynImuzIadlrJ6jJ1s6rk8vqoTb3v8A7M9Hqcv7mLXSqOyTvbgthdOTmnZ56gNDsBdxq6AWNCUBFolDURoCKChiAChCQABAkAAShkg0AKQNBAN/o+OLyLqfCte7PTwzpPbg8lo4NziuN+fCPQqCSTW6ryS5J66+C6js4cqZd1nnNJmn9Rrd0uVw0drDNtWyNx068M9s+qwK2/JzNRjq6/Q6+eRzMuFve6W9+5rFPkjzmVU3fkrLdQ93W+7KmdLz6hCBSAgaFaNeHSuW36jZdDS53+BbjXW62w0SiyUGgRhfgbBCUNQzgAV0FIZxCALQaNvpug+q3vSS5q9yjUYXCThs68bi37prrdbU0Q6ek9N614d13ZBXORucWd9jJgtW123vwzs4dd+WpLdpUq5OLjdJ8/Hk6/peeLnC/FfAs43xX3T0ejwx6bS7XwPKBfFKthJs5Xo6YM6MOSPVcXxwzpZjBLaRvFLJjl6bDxRRqdDGVdq245OpPcR4zcyqd45fxwNbouiq3239zPhxOX9qs9Jk0nXtdD6b0+MFVeLfk1/Z4jeDd/xk0WnpKVNPun5F1VJ268I7qW3HbueZ1GHI5vqTpN9qVexnG7qmePWSRXlwd1358FK0bpyXK3r2NOSdKq5Jjb/t7vb7FPULJtghDq7Fv0nWye4sm4trwb9Nq10u1vQ7tjGS/XNWNtqJ2dF6RF03v9zFhwNtSb2vlNHotDPs6MZ5X8W4eOW+tmn0ySpIaHp8E7pWX4GaYo5913zGMuHRxXCS77KiGuyCa1HzWOF+DrelaH8yd+/3MWKe6tOvk9To1HpTR055WR5/Dxy3bbF0iubGcjPPJ/77HO7rS5WZZounMolI1E6iG6SuL3uzTEdKJgpclkpJvgShowEa3Ffcp1MduOS1WivLK0xHfjzWbSzc2oq6t7+DPkhNbtPbb4O5ji1Jya52GyQ3vz2LTNyXi280aMORKrOrj9G6re6t2vjwaof0/F8t8Uunb7mryYszgz/HAeTnpTf+Ds+naWaipzb3SpJfydDF6HBKvbnubcenpKPhUTy5Jfi/Hw2XdU6Gd+1HQjIzY9PTtF8CVdOPh7II2QRvCY5I6WLUOklytl8GHBhvc34NPunxf8djpy04OPbp6nUNQ2e/dnOwavd/52s2ZcdpK6OVr9P0737GMZFuS2eujHURa6r42+5QtVHz7HHU3x5L9PhbkrNdNJTltdTTvd7cM6EUZ8EUkXxmTrox8OhoidZGzLQzkVuRnzze7Rj/ABXN9jUxYyz0vy59+UtwrWRi6pN++xycytp7r/RTFNyfcp1iF5bK9hpMikr/AENkGcn0jE1Gm/j2R1ISI5fXZhdxcgMliZHsZaS9xqKcT3NCYBHEAxAN4LDKmasGo3dvau/JR0lbR1a28yWxqerbddkWZ8icXe5gCHU+9JZ0NPniq8mFofFCx2bZxtldfT5r4NkYnH0Kbklb9/g7+LHsRy8dfHe0U9JGaHEpyIwpYyZpJMzajJHv34LtVFv4OdnW6K4xDPLRpK1f2G9N0VybfY2YNOmro2aXHXYVy8GPHuy1tww4L1ArwlyI11w0UCUSyIWBs/0h0hxZoAlkEsgB4mLI0PlwuPIh1PMpXEFFgrQyKRBoKQE3+l6hRdSr2Z1sesi+HZ5ui7Fl6TGWG18OWyaej6zJqtRTow49fSY8MvU7ff8AYx10reTc8aG7VlP4dMpnadKy2GV7Iemdy/WrDtsacUjA8xs0j7mbFMa34kXRRTAtRNaHsliEcgM7YjYrmL1ARglbmQA4mswpo5meFUdLJP8AKc6e50YuLk0qIiNENomURugrQ8ZAcRxA0OShBX0hjJotkytoBprwxbX8DONUUYc1bF2bLw0ZsVlmliS7mzTZ0tjnrJtbK8bbYuu2pnr49LinZb1HLwamqRr+oRsdOOW18plf1CqWQqlMNC5NEplbyFE8lFL1CNSM3NqnlIc/NlCPqneRXCVr7mKa3fyaMT2def2KcnLKRHK7kVMFF30XVldGk7C0Qt+mxekBoqYyYeh1YEgBgyiCKGbA1fSFRYxEAXJqip7AbFbFo7VkZvya8er8mFMKYWbEysa56t2Ceo3+DI5AsXWH3q6WobKnMRgNaYuVWPKyFZA0W213HsCcU033L8rbEqtjC9ivBkpNMmVXwhorcuhHjZe4bEm5pVjw7GiOkRrxw4LpRMXJXHjjDPGkkjn6rA4vtv4Oy4Jv4E1cFJVwOZaLPDccIhqzaat0zN0lZduaywoyZKCBIBhIBhQBwUBFYo7QKAqUA1E6e4wQgaIMnWnJCKZWwxRLTp3tYi6JnLYzFTla8MixZLMayIk8tbmNKdtNMoleWPYkc6LYTTA/Kx5NPddgw0aXk1/UQepD7UukYsuBU6W5z3Hk7UpJGHJTba+5rGp8mMYqBRbkjQtG9oaKFINEAA0LRYSgCpoiRZ0g6RhU0QeUSAzpqQ1AiGzC5WQgaACmJJjAaAJGbLMWSiogaKVpUy/r2+THBl8JGbFMaGUyTZtkv3MmXkcZzVMFDUSjaRKDQaJQEBAkABZEyEoALRAEANEhQhMqloISARSDURoAWiBoaMQARLIiUQDh5tlLHFAqRonSPRKGWidJKOlj9Km93Ud+/cz6vSSxun34a4Yu0O4WTdjJQKLKIsbfCY9s6V0Sh3AFDLRaINRAJdKItF7QjRlXSsIxAICBCBlCRlqguhvvdAaoKQAgQ0BpBAAL0nW9P9OrpnLnlLx8mTQRTyRv3/g9BDgxnl+LcWEvtCRzPUcEp0orhts6kxIrYnLpbKdpp5pad3TTX22NsssYr+DoZ4p0nxZw5r81dlJr7WUl7Oezp8N9C0nvbZTlxuLpnXitkV5IJ3avkJkLx+M3p+heTe6SdPz9gHW9JxpRlXd/4IZyyu1cOLHXr//Z',};
    return (
        <Image style={styles.imageBackground} source={background} />
    )
}
  
const styles = StyleSheet.create({
    logo: {
        padding: 10,
        width: 200,
        height: 50,
        justifyContent: 'center',
        alignContent: 'center',
    },
    imageBackground: {
      flex: 1,
      resizeMode: 'cover',
      justifyContent: 'center',
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },
});
  