import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

export const RegisterButton = ({title, onPress}) => {
  return (
    <TouchableOpacity style={styles.registerButton} onPress={onPress}>
      <View>
        <Text style={styles.textButton}>{title}</Text>
      </View>
    </TouchableOpacity>
  )
}

export const LoginButton = ({title, onPress}) => {
  return (
    <TouchableOpacity style={styles.loginButton} onPress={onPress}>
      <View>
        <Text style={styles.textButton}>{title}</Text>
      </View>
    </TouchableOpacity>
  )
}
export const LinkButton = ({title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.link}>{title}</Text>
    </TouchableOpacity>
  )
}

  
const styles = StyleSheet.create({
    textButton: {
      color: 'white',
      textAlign: 'center',
      padding: 5,
      fontSize: 15,
    },
    loginButton: {
      width: 200,
      backgroundColor: '#003366',
      paddingHorizontal: 10,
      paddingVertical: 5,
      borderRadius: 10,
      margin: 5,
      textAlign: 'center',
    },  
    registerButton: {
      width: 200,
      backgroundColor: '#3EC6FF',
      paddingHorizontal: 10,
      paddingVertical: 5,
      borderRadius: 10,
      margin: 5,
      textAlign: 'center',
    },        
    link: {
      color: 'white',
      fontSize: 18,
      fontWeight: 'bold',
      textAlign: 'center',
    },
  });
  