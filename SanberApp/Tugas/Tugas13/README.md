# Tugas 13 - React Native Styling dan Flex

## Register
![Register Page](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/SanberApp/Tugas/Tugas13/screenshots/Register.jpeg "Register Page")


## Login
![Login Page](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/SanberApp/Tugas/Tugas13/screenshots/Login.jpeg "Login Page")


## About
![About Page](https://gitlab.com/fahryrozy/bootcampreactnative/-/tree/master/SanberApp/Tugas/Tugas13/screenshots/About.jpeg "About Page")
