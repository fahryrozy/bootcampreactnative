console.log("Tugas 3 - Looping");
console.log("====================\n");

console.log("No. 1 Looping While");
let i = 1
let j = 1
console.log("Looping Pertama");
while(i<=10) {
  console.log(i*2 + ' - I love coding')
  i++;
}
console.log("Looping Kedua");
while(j<i) {
  console.log((i-j)*2 + ' - I will become a mobile developer')
  j++;
}

console.log("\nNo. 2 Looping menggunakan for");
let k;
for(k=1; k<=20; k++) {
    if(k%3 == 0) {
        if(k%2 != 0)
        console.log(k+' - I Love Coding');
        else {
            console.log(k+' - Berkualitas');
        }
    }
    else if(k%2==0) {
        console.log(k+' - Berkualitas');
    }
    else {
        console.log(k+' - Santai');        
    }
}


console.log("\nNo. 3 Membuat Persegi Panjang #");
for(let i=0; i<4; i++) {
    let box = ''
    for(let j=0; j<8; j++) {
        box += '#'
    }
    console.log(box);
}

console.log("\nNo. 4 Membuat Tangga ");
let stair = ''
for(let i=0; i<7; i++) {
    stair += '#'
    console.log(stair);
}


console.log("\nNo. 5 Membuat Papan Catur");
for(let i=0; i<8; i++) {
    let checkered = '';
    if(i%2 != 0) {
        for(let j=0; j<8; j++) {
            if(j%2 != 0) {
                checkered += ' ';
            }else {
                checkered += '#'
            }
        }
        console.log(checkered);
    }
    else {
        for(let j=0; j<8; j++) {
            if(j%2 != 0) {
                checkered += '#';
            }else {
                checkered += ' '
            }
        }
        console.log(checkered);
    }
}
