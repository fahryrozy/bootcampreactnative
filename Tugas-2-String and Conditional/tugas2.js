console.log('Tugas 2 - String & Conditional');
console.log('================================');

console.log('***A. String***');

//Soal No. 1 (Membuat kalimat)
// Output: JavaScript is awesome and I love it! 
console.log('Soal No. 1');
console.log('===========');
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

const joinWord = word + ' ' + second + ' ' + third + ' ' + fourth + ' ' + sixth + ' ' + seventh

console.log(joinWord + '\n')

//Soal No.2 Mengurai kalimat (Akses karakter dalam string)
// Buat menjadi Output berikut:

// First word: I 
// Second word: am 
// Third word: going 
// Fourth word: to 
// Fifth word: be 
// Sixth word: React 
// Seventh word: Native 
// Eighth word: Developer
console.log('Soal No. 2');
console.log('===========');
var sentence = "I am going to be React Native Developer"; 

var firstWord = sentence[0]; 
var secondWord = sentence[2]+sentence[3]; 
var thirdWord = sentence[4]+sentence[5]+sentence[6]+sentence[7]+sentence[8]+sentence[9];
var fourthWord = sentence[11]+sentence[12];
var fifthWord = sentence[14]+sentence[15];
var sixthWord = sentence[16]+sentence[17]+sentence[18]+sentence[19]+sentence[20]+sentence[21];
var seventhWord = sentence[23]+sentence[24]+sentence[25]+sentence[26]+sentence[27]+sentence[28];
var eighthWord = sentence[30]+sentence[31]+sentence[32]+sentence[33]+sentence[34]+sentence[35]+sentence[36]+sentence[37]+sentence[38];

console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord + '\n');

// Soal No. 3 Mengurai Kalimat (Substring)
// Uraikan lah kalimat sentence2 di atas menjadi kata-kata penyusunnya. Output:

// First Word: wow 
// Second Word: JavaScript 
// Third Word: is 
// Fourth Word: so 
// Fifth Word: cool 
console.log('Soal No. 3');
console.log('===========');
var sentence2 = 'wow JavaScript is so cool'; 

var firstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);  
var thirdWord2 = sentence2.substring(15, 17);  
var fourthWord2 = sentence2.substring(18, 20);  
var fifthWord2 = sentence2.substring(21, 25);  

console.log('First Word: ' + firstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2 + '\n');

// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
// Output:
// First Word: wow, with length: 3 
// Second Word: JavaScript, with length: 10 
// Third Word: is, with length: 2 
// Fourth Word: so, with length: 2 
// Fifth Word: cool, with length: 4
console.log('Soal No. 4');
console.log('===========');

var sentence3 = 'wow JavaScript is so cool'; 

var firstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);  
var thirdWord3 = sentence3.substring(15, 17);  
var fourthWord3 = sentence3.substring(18, 20);  
var fifthWord3 = sentence3.substring(21, 25);  

var firstWordLength = firstWord3.length 
var secondWord3Length = secondWord3.length 
var thirdWord3Length = thirdWord3.length 
var fourthWord3Length = fourthWord3.length 
var fifthWord3Length = fifthWord3.length  
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + firstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3Length); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3Length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3Length); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3Length + '\n'); 

console.log('----------------------------');

console.log('***B. Conditional***');

//Soal No. 1 - If Else
console.log('Soal No. 1 - If Else Condition');
console.log('================================');
var nama = prompt('Hi, silahkan masukkan nama kamu? =').toLowerCase()
var peran = prompt('sebutkan peran kamu? =').toLowerCase()
if(nama=='' && peran == '') {
  console.log("Nama harus diisi!");
}
else if(nama=='john' && peran == '') {
  console.log("Halo John, Pilih peranmu untuk memulai game!");
}
else if(nama=='jane' && peran == 'penyihir') {
  console.log("Selamat datang di Dunia Werewolf, Jane");
  console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
}
else if(nama=='jenita' && peran == 'guard') {
  console.log("Selamat datang di Dunia Werewolf, Jenita");
  console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
}

else if(nama=='junaedi' && peran == 'werewolf') {
  console.log("Selamat datang di Dunia Werewolf, Junaedi");
  console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
}
else {
  console.log("Nama dan peran salah");
}

console.log('')
console.log('Soal No. 2 - Switch Condition');
console.log('===============================');
var hari = prompt('Masukkan tanggal antara 1 - 31 ='); 
var bulan = prompt('Masukkan bulan antara 1 - 12 ='); 
var tahun = prompt('Masukkan tahun antara 1900 - 2020 =');

switch(bulan) {
  case '1':
    bulan = 'Januari';
    break;
  case '2':
    bulan = 'Februari';
    break;
  case '3':
    bulan = 'Maret';
    break;
  case '4':
    bulan = 'April';
    break;
  case '5':
    bulan = 'Mei';
    break;
  case '6':
    bulan = 'Juni';
    break;
  case '7':
    bulan = 'Juli';
    break;
  case '8':
    bulan = 'Agustus';
    break;
  case '9':
    bulan = 'September';
    break;
  case '10':
    bulan = 'Oktober';
    break;
  case '11':
    bulan = 'November';
    break;
  case '12':
    bulan = 'Desember';
    break;
  default:
    bulan = '(Salah Input Bulan)';
}
console.log(hari + ' ' + bulan + ' ' + tahun);